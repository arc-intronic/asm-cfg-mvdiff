#![forbid(unsafe_code)]

use std::collections::HashSet;
use std::collections::HashMap;

use std::collections::hash_map::DefaultHasher;

use std::fs::File;

use std::hash::Hasher;

use std::io::ErrorKind;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;

use std::marker::PhantomData;

use std::str::FromStr;

use lazy_static::lazy_static;

use regex::Regex;

use rand::Rng;

fn main() -> Result<(), std::io::Error> {
    let fname: &'static str = "testgawk.asm";
    let disasm_init = PreparedDisassembly::try_new(fname)?;
    let mut disasm = disasm_init.try_upgrade()?;
    let results = AsmBlock::build_at(&mut disasm, 0x0_u64, HashSet::new())?;
    println!("{:?}", results[0].uses);
    Ok(())
}

fn test_parsing() -> Result<(), std::io::Error> {
    println!("[INFO] -APP:DEVEL:TEST Started...");
    let example_computation = "000B15E2  086732            or [bx+0x32],ah";
    let example_computation_parsed: Instruction = example_computation.parse()?;
    let example_computation_parsed_target = Instruction::ComputeAndWrite(
        CompInstruction {
            op: Op("or".to_owned()),
            sets: vec![
                Reg::PtrSum(
                    vec![
                        RegBase::Named("bx".to_owned()),
                        RegBase::Fixed(0x32_usize as isize)
                    ]
                )
            ].into_iter().collect::<HashSet<_>>(),
            gets: vec![
                Reg::PtrSum(
                    vec![
                        RegBase::Named("bx".to_owned()),
                        RegBase::Fixed(0x32_usize as isize)
                    ]
                ),
                Reg::Base(
                    RegBase::Named("ah".to_owned())
                )
            ].into_iter().collect::<HashSet<_>>(),
        }
    );
    let example_unconditional_jump = "000B15E2  086732            jmp 0x7A";
    let example_unconditional_jump_parsed: Instruction = example_unconditional_jump.parse()?;
    let example_unconditional_jump_parsed_target = Instruction::ControlFlow(
        JumpComponent::Unconditional(
            Reg::Base(
                RegBase::Fixed(0x7A_isize)
            )
        )
    );
    println!("{:?}", example_computation_parsed);
    assert_eq!(example_computation_parsed, example_computation_parsed_target);
    println!("{:?}", example_unconditional_jump_parsed);
    assert_eq!(example_unconditional_jump_parsed, example_unconditional_jump_parsed_target);
    println!("[INFO] -APP:DEVEL:TEST Completed.");
    Ok(())
}

impl AsmBlock {
    fn build_at<'a, 'b : 'a>(
        disasm: &'a mut PreparedDisassembly<'b, FileKnown<'b>>,
        index: u64, active: HashSet<u64>
    )
        -> Result<Vec<Self>, std::io::Error>
    {
        let mut cursor = index;
        let mut regs_inp: HashSet<Reg> = HashSet::new();
        let mut regs_out: HashSet<Reg> = HashSet::new();
        let mut uses: Vec<u64> = Vec::new();
        let mut blocks_acc: Vec<AsmBlock> = Vec::new();
        disasm.seek_direct(index)?;
        let mut ccode: Vec<CompInstruction> = Vec::new();
        loop {
            if let Ok(instr) = disasm.read_program_item() {
                match instr {
                    Instruction::ComputeAndWrite(overall) => {
                        ccode.push(overall.clone());
                        match overall {
                            CompInstruction { op: _, ref sets, ref gets } => {
                                regs_inp.extend(sets.clone().iter().map(|x| x.clone()));
                                regs_out.extend(gets.clone().iter().map(|x| x.clone()));
                            }
                        }
                    },
                    Instruction::FnControlFlow(FnJump::Call(target)) => {
                        if let Some(p_usize) = target.resolve_simple() {
                            let p = {
                                let p_in = disasm.locations.get(&p_usize).ok_or(
                                    std::io::Error::new(ErrorKind::NotFound, format!("Cannot find assoc location for {:?}", p_usize))
                                )?;
                                *p_in
                            };
                            if active.contains(&p) {
                                let blk = AsmBlock {
                                    regs_inp,
                                    regs_out,
                                    pos_begin: index,
                                    pos_final: cursor,
                                    uses,
                                    ccode,
                                };
                                blocks_acc.insert(0, blk);
                                return Ok(blocks_acc);
                            }
                            let result = Self::build_at(
                                disasm, p, {
                                    let mut active_new = active.clone();
                                    active_new.insert(index);
                                    active_new
                                }
                            )?;
                            let fnblock = result[0].clone();
                            regs_inp.extend(fnblock.regs_inp.iter().map(|x| x.clone()));
                            regs_out.extend(fnblock.regs_out.iter().map(|x| x.clone()));
                            uses.push(p);
                            blocks_acc.extend(result);
                        } else {
                            let blk = AsmBlock {
                                regs_inp,
                                regs_out,
                                pos_begin: index,
                                pos_final: cursor,
                                uses,
                                ccode,
                            };
                            blocks_acc.insert(0, blk);
                            return Ok(blocks_acc);
                        }
                    },
                    Instruction::FnControlFlow(FnJump::Ret) => {
                        let blk = AsmBlock {
                            regs_inp,
                            regs_out,
                            pos_begin: index,
                            pos_final: cursor,
                            uses,
                            ccode,
                        };
                        blocks_acc.insert(0, blk);
                        return Ok(blocks_acc);
                    },
                    Instruction::ControlFlow(JumpComponent::Unconditional(target)) => {
                        if let Some(p_usize) = target.resolve_simple() {
                            let p = {
                                let mut g = p_usize;
                                let p_in;
                                loop {
                                    if let Some(p_in2) = disasm.locations.get(&g) {
                                        p_in = p_in2;
                                        break;
                                    }
                                    g += 1;
                                }
                                *p_in
                            };
                            cursor = p;
                            disasm.seek_direct(cursor)?;
                        } else {
                            let blk = AsmBlock {
                                regs_inp,
                                regs_out,
                                pos_begin: index,
                                pos_final: cursor,
                                uses,
                                ccode,
                            };
                            blocks_acc.insert(0, blk);
                            return Ok(blocks_acc);
                        }
                    },
                    Instruction::ControlFlow(JumpComponent::Conditional { target, op: _, gets }) => {
                        for ro in regs_out.iter() {
                            let mut gets_hashset = HashSet::from_iter(gets.iter().cloned());
                            gets_hashset.insert(ro.clone());
                            ccode.push(
                                CompInstruction {
                                    sets: HashSet::from_iter(std::iter::repeat(ro.clone()).take(1)),
                                    gets: gets_hashset,
                                    op: Op("FlagConditional_".to_owned()),
                                }
                            );
                        }
                        if rand::thread_rng().gen_range(0..2) == 0 {
                            if let Some(p_usize) = target.resolve_simple() {
                                let p = {
                                    let mut g = p_usize;
                                    let p_in;
                                    loop {
                                        if let Some(p_in2) = disasm.locations.get(&g) {
                                            p_in = p_in2;
                                            break;
                                        }
                                        g += 1;
                                    }
                                    *p_in
                                };
                                cursor = p;
                                disasm.seek_direct(cursor)?;
                            } else {
                                let blk = AsmBlock {
                                    regs_inp,
                                    regs_out,
                                    pos_begin: index,
                                    pos_final: cursor,
                                    uses,
                                    ccode,
                                };
                                blocks_acc.insert(0, blk);
                                return Ok(blocks_acc);
                            }
                        }
                    },
                }
            }
            cursor += 1_u64;
        }
    }

    /*
    fn build_oldver<'a>(disasm: &'a mut PreparedDisassembly<'a, FileKnown<'a>>) -> Self {
        let (tx, rx) = std_mpsc::channel::<AsmBlock>();
        let mut track_next: Vec<TargetCFG> = Vec::new();
        let acc_thread = thread::spawn(
            move || {
                let mut res: Vec<AsmBlock> = Vec::new();
                let mut blocks_tracked: HashMap<(u64, u64), usize> = HashMap::new();
                let mut new_index = 0_usize;
                for i in rx {
                    let cpositions = (i.pos_begin, i.pos_final);
                    if let Some(res_index) = blocks_tracked.get(&cpositions) {
                        res[*res_index].possible_sources.push(possible_sources[0].clone());
                    } else {
                        res.push(i);
                        blocks_tracked.insert(cpositions, new_index);
                        new_index += 1_usize;
                    }
                }todo!()
                BlockCFG(res)
            }
        );
        while track_next.len() > 0_usize {
            for TargetCFG { source: (srcnum, mv), position } in track_next.clone() {
                // TODO1 TODO1 TODO1
                // goto position
                // iterate until find end of block
                // pack everything up and send with tx
                let mut cursor = position;
                let mut regs_inp = Vec::new();
                let mut regs_hashset_w = HashSet::new();
                let cf_final;
                loop {
                    disasm.seek_direct(cursor);
                    if let Ok(instr) = disasm.read_program_item() {
                        match instr {
                            Instruction::ComputeAndWrite(CompInstruction { op: _, sets, gets }) => {
                                for inp in gets.difference(&regs_hashset_w) {
                                    regs_inp.push(inp.clone());
                                }
                                for w_target in sets {
                                    regs_hashset_w.insert(w_target);
                                }
                            },
                            Instruction::ControlFlow(jump_component) => {
                                cf_final = jump_component;
                                break;
                            }
                        }
                    }
                };
                // TODO process cf_final -> track_next
                tx.send(
                    AsmBlock {
                        regs_inp,
                        regs_out: None,
                        pos_begin: position,
                        pos_final: cursor,
                        possible_sources: vec![
                            (srcnum, mv),
                        ],
                    }
                ).unwrap();
            }
        }
        acc_thread.join().unwrap()
}*/
    // */
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Clone)]
struct TargetCFG { //#     source: (usize, MvDesc),
    position: u64,
}

#[derive(Debug)]
#[derive(Clone)]
struct BlockCFG(Vec<AsmBlock>);

#[derive(Debug)]
#[derive(Clone)]
struct AsmBlock {
    regs_inp: HashSet<Reg>,
    regs_out: HashSet<Reg>,
    pos_begin: u64,
    pos_final: u64, //&    possible_sources: Vec<(usize, MvDesc)>,
    uses: Vec<u64>,
    ccode: Vec<CompInstruction>,
}
/*
#[derive(Debug)]
#[derive(PartialEq, Eq, Clone)]
struct MvDesc(HashSet<Switchpoint>);

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Clone)]
struct Switchpoint(Op, RegRef, bool);
 */

impl Reg {
    fn resolve_simple(self) -> Option<usize> {
        match self {
            Reg::Base(RegBase::Fixed(isize_)) => {
                Some(isize_ as usize)
            },
            _ => None,
        }
    }
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Clone)]
enum Instruction {
    ComputeAndWrite(CompInstruction),
    ControlFlow(JumpComponent),
    FnControlFlow(FnJump),
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Clone)]
enum FnJump {
    Call(Reg),
    Ret,
}

type StdHash = u64;

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Clone)]
enum JumpComponent {
    Conditional {
        target: Reg,
        op: StdHash,
        gets: Vec<Reg>,
    },
    Unconditional(Reg),
}

trait ProgramRead {
    type ReadError;
    type ProgramItem;
    fn read_program_item(&mut self) -> Result<Self::ProgramItem, Self::ReadError>;
}

trait ProgramSeek: ProgramRead {
    type SeekError;
    fn seek_program(&mut self, _: usize) -> Result<(), Self::SeekError>;
    fn seek_direct(&mut self, _: u64) -> Result<(), Self::SeekError>;
}

impl ProgramRead for PreparedDisassembly<'_, FileKnown<'_>> {
    type ReadError = std::io::Error;
    type ProgramItem = Instruction;
    fn read_program_item(&mut self) -> Result<Self::ProgramItem, Self::ReadError> {
        let mut buf: Vec<u8> = Vec::new();
        loop {
            let mut curr_item = [0x00_u8; 1];
            self.fpath.file.read_exact(&mut curr_item)?;
            let curr_byte = curr_item[0];
            if curr_byte == 0x0A_u8 {
                break;
            } else {
                buf.push(curr_byte);
            }
        }
        let text = from_utf8_io_result(buf)?;
        text.parse()
    }
}

fn from_utf8_io_result(vec: Vec<u8>) -> Result<String, std::io::Error> {
    match std::str::from_utf8(&vec) {
        Ok(x) => Ok(x.to_owned()),
        Err(utf8_err) => Err(
            std::io::Error::new(ErrorKind::InvalidData, format!("(utf8_err is) {}", utf8_err))
        ),
    }
}

impl FromStr for Instruction {
    type Err = std::io::Error;
    fn from_str(s: &str) -> Result<Self, <Self as FromStr>::Err> {
        let instruction_part = &s[28..];
        let ws_split_vec = instruction_part
            .split_whitespace()
            .collect::<Vec<_>>();
        let (instr, ws_split_remaining) = ws_split_vec.split_at(1);
        if instr[0] == "jmp" {
            return Ok(
                Instruction::ControlFlow(
                    JumpComponent::Unconditional(
                        ws_split_remaining
                            .into_iter()
                            .next()
                            .ok_or(
                                std::io::Error::new(
                                    ErrorKind::InvalidData,
                                    "Unconditional jump (jmp) with no target"
                                )
                            )?
                            .parse()?
                    )
                )
            );
        }
        let instr_op = Op(instr[0].to_owned());
        let remaining = ws_split_remaining.join("");
        let mut operands = remaining.split(",");
        if let Some(args) = instr_op.conditional_jump() {
            return Ok(
                Instruction::ControlFlow(
                    JumpComponent::Conditional {
                        target: {
                            operands
                                .next()
                                .expect("Conditional jump with no target")
                                .parse()?
                        },
                        op: {
                            let mut hasher = DefaultHasher::new();
                            hasher.write(instr_op.0.as_bytes());
                            hasher.finish()
                        },
                        gets: {
                            let mut gets: Vec<Reg> = Vec::new();
                            gets.extend(args);
                            for i in operands {
                                gets.push(i.parse()?);
                            }
                            gets
                        },
                    }
                )
            );
        }
        let remaining = ws_split_remaining.join("");
        let mut operands = remaining.split(",").peekable();
        let mut instruction_sets = HashSet::new();
        let mut instruction_gets = HashSet::new();
        match operands.peek() {
            Some(x) => {
                instruction_sets.insert(x.parse()?);
                ()
            },
            None => {
                ()
            },
        };
        for i in operands {
            instruction_gets.insert(i.parse()?);
        }
        Ok(
            Instruction::ComputeAndWrite(
                CompInstruction {
                    op: Op(instr[0].to_owned()),
                    sets: instruction_sets,
                    gets: instruction_gets,
                }
            )
        )
    }
}

impl Op {
    fn conditional_jump(&self) -> Option<Vec<Reg>> {
        if self.0.starts_with(&['j'][..]) {
            if &self.0[..] == "jmp" {
                None
            } else {
                Some(vec![Reg::Base(RegBase::Named("CMPFLAG".to_owned()))])
            }
        } else { None }
    }
}

impl FromStr for CompInstruction {
    type Err = std::io::Error;
    fn from_str(s: &str) -> Result<Self, <Self as FromStr>::Err> {
        let instruction_part = &s[28..];
        let ws_split_vec = instruction_part
            .split_whitespace()
            .collect::<Vec<_>>();
        let (instr, ws_split_remaining) = ws_split_vec.split_at(1);
        let remaining = ws_split_remaining.join("");
        let mut operands = remaining.split(",").peekable();
        let mut instruction_sets = HashSet::new();
        let mut instruction_gets = HashSet::new();
        match operands.peek() {
            Some(x) => {
                instruction_sets.insert(x.parse()?);
                ()
            },
            None => {
                ()
            },
        };
        for i in operands {
            instruction_gets.insert(i.parse()?);
        }
        if instr[0] == "cmp" || instr[0] == "test" {
            instruction_sets.insert(Reg::Base(RegBase::Named("CMPFLAG".to_owned())));
        }
        Ok(
            CompInstruction {
                op: Op(instr[0].to_owned()),
                sets: instruction_sets,
                gets: instruction_gets,
            }
        )
    }
}

impl FromStr for Reg {
    type Err = std::io::Error;
    fn from_str(s: &str) -> Result<Self, <Self as FromStr>::Err> {
        lazy_static! {
            static ref REGEX_PTR_SUM: Regex = Regex::new(
                r#"\[\w+([\+\-]\w+)*\]"#
            ).unwrap();
        }
        Ok(
            if s.chars().all(|c| c.is_alphanumeric()) {
                Reg::Base(s.parse()?)
            } else {
                if REGEX_PTR_SUM.is_match(s) {
                    let corrected_sum_expr = (&s[1..(s.len() - 1)])
                        .replace("-", "-~");
                    let reg_base_parts = corrected_sum_expr
                        .split(&['+', '-'][..]);
                    let mut sum_definition: Vec<RegBase> = Vec::new();
                    for i in reg_base_parts {
                        sum_definition.push(i.parse()?);
                    }
                    Reg::PtrSum(sum_definition)
                } else {
                    let components = s
                        .split(|c: char| !c.is_alphanumeric())
                        .filter(|x| *x != "");
                    let mut components_parsed: Vec<RegBase> = Vec::new();
                    for i in components {
                        components_parsed.push(i.parse()?)
                    }
                    Reg::PtrArithOther(components_parsed)
                }
            }
        )
    }
}

impl FromStr for RegBase {
    type Err = std::io::Error;
    fn from_str(s: &str) -> Result<Self, <Self as FromStr>::Err> {
        let empty_error = std::io::Error::new(ErrorKind::InvalidInput, "Cannot parse \"\" as RegBase");
        Ok(
            if s.chars().next().ok_or(empty_error)?.is_numeric() {
                RegBase::Fixed(parse_general_isize(s)?)
            } else {
                RegBase::Named(s.to_owned())
            }
        )
    }
}

fn parse_general_isize(s: &str) -> Result<isize, std::io::Error> {
    let empty_error = std::io::Error::new(ErrorKind::InvalidInput, "Cannot parse \"\" as any isize");
    let mut char_iter = s.chars();
    let first_char = char_iter.next().ok_or(empty_error)?;
    if first_char == '~' {
        Ok(
            -parse_general_isize(&s[1..])?
        )
    } else {
        Ok(
            parse_general_usize_informed(s, first_char, char_iter)? as isize
        )
    }
}

fn parse_general_usize(s: &str) -> Result<usize, std::io::Error> {
    let empty_error = std::io::Error::new(ErrorKind::InvalidInput, "Cannot parse \"\" as any isize");
    let mut char_iter = s.chars();
    let first_char = char_iter.next().ok_or(empty_error)?;
    parse_general_usize_informed(s, first_char, char_iter)
}

fn parse_general_usize_informed(
    s: &str,
    first_char: char,
    mut char_iter: impl Iterator<Item = char>
) -> Result<usize, std::io::Error> {
    if first_char != '0' {
        match s.parse() {
            Ok(x) => Ok(x),
            Err(parse_error) => Err(
                std::io::Error::new(
                    ErrorKind::InvalidData,
                    format!("(parse_error is) {}", parse_error)
                )
            )
        }
    } else {
        Ok(
            match char_iter.next() {
                Some('x') => usize::from_str_radix_io(&s[2..], 16)?,
                Some('b') => usize::from_str_radix_io(&s[2..], 2)?,
                Some(..) => usize::from_str_radix_io(s, 8)?,
                None => 0x0_usize,
            }
        )
    }
}

trait FromStrRadixIO where Self: Sized {
    fn from_str_radix_io(src: &str, radix: u32) -> Result<Self, std::io::Error>;
}

impl FromStrRadixIO for usize {
    fn from_str_radix_io(src: &str, radix: u32) -> Result<Self, std::io::Error> {
        if src.trim().is_empty() {
            return Ok(0x0_usize);
        }
        match Self::from_str_radix(src, radix) {
            Ok(x) => Ok(x),
            Err(parse_error) => Err(
                std::io::Error::new(
                    ErrorKind::InvalidData,
                    format!("(:::from_str_radix_io/parseerror parse_error) {}", parse_error)
                )
            ),
        }
    }
}

impl FromStrRadixIO for isize {
    fn from_str_radix_io(src: &str, radix: u32) -> Result<Self, std::io::Error> {
        Ok(
            usize::from_str_radix_io(src, radix)? as isize
        )
    }
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Clone)]
struct CompInstruction {
    op: Op,
    sets: HashSet<Reg>,
    gets: HashSet<Reg>,
}

impl ProgramSeek for PreparedDisassembly<'_, FileKnown<'_>> {
    type SeekError = std::io::Error;
    fn seek_program(&mut self, target: usize) -> Result<(), Self::SeekError> {
        let target_bytes = self.locations
            .get(&target)
            .ok_or(
                std::io::Error::new(ErrorKind::NotFound, "Could not find usize")
            )?;
        self.fpath.file.seek(
            SeekFrom::Start(*target_bytes)
        )?;
        Ok(())
    }

    fn seek_direct(&mut self, target: u64) -> Result<(), Self::SeekError> {
        self.fpath.file.seek(
            SeekFrom::Start(target)
        )?;
        Ok(())
    }
}

#[derive(Debug)]
#[derive(Clone)]
struct PreparedDisassembly<'a, R = &'a str> where R: FileRef {
    fpath: R,
    locations: DisassemblyLocationMap,
    phantom: PhantomData<&'a str>,
}

type DisassemblyLocationMap = HashMap<usize, u64>;

impl<'a> PreparedDisassembly<'a> {
    fn try_new(fpath: &'a str) -> Result<Self, std::io::Error> {
        let locations = derive_file_locations(fpath)?;
        Ok(
            Self {
                fpath,
                locations,
                phantom: PhantomData,
            }
        )
    }
}

impl<'a> PreparedDisassembly<'a> {
    fn try_upgrade(&'a self) -> Result<PreparedDisassembly<'a, FileKnown<'a>>, std::io::Error> {
        let file_known = self.fpath.refers_to_file()?;
        Ok(
            PreparedDisassembly::<'a, FileKnown<'a>> {
                fpath: file_known,
                locations: self.locations.clone(),
                phantom: PhantomData,
            }
        )
    }
}

fn derive_file_locations(fpath: &str) -> Result<DisassemblyLocationMap, std::io::Error> {
    let mut locations: DisassemblyLocationMap = HashMap::new();
    let mut reader = reader_noalloc::BufReader::open(fpath)?;
    let mut buffer = String::new();
    let mut last_index = 0_u64;
    while let Some(line) = reader.read_line(&mut buffer).transpose()? {
        let index_word: &str = &line[..8];
        let assembly_index = usize::from_str_radix_io(index_word, 16_u32)?;
        locations.insert(assembly_index, last_index);
        last_index = reader.get_position_bytes();
    }
    Ok(locations)
}

mod reader_noalloc {
    use std::fs::File;
    use std::io;
    use std::io::prelude::*;

    pub struct BufReader {
        reader: io::BufReader<File>,
    }

    impl BufReader {
        pub fn open(path: impl AsRef<std::path::Path>) -> io::Result<Self> {
            let file = File::open(path)?;
            let reader = io::BufReader::new(file);
            Ok(
                Self { reader }
            )
        }

        pub fn read_line<'buf>(
            &mut self,
            buffer: &'buf mut String,
        ) -> Option<io::Result<&'buf mut String>> {
            buffer.clear();
            self.reader
                .read_line(buffer)
                .map(|u| if u == 0 { None } else { Some(buffer) })
                .transpose()
        }

        pub fn get_position_bytes(&mut self) -> u64 {
            self.reader.seek(io::SeekFrom::Current(0_i64)).expect("No-op seek failed")
        }
    }
}

trait FileRef {
    fn refers_to_file(&self) -> Result<FileKnown, std::io::Error>;
    fn refers_to_file_at(&self) -> &str;
}

impl FileRef for String {
    fn refers_to_file(&self) -> Result<FileKnown, std::io::Error> {
        let fh = File::open(&self)?;
        Ok(
            FileKnown {
                fpath: &self,
                file: fh,
            }
        )
    }

    fn refers_to_file_at(&self) -> &str {
        &self
    }
}

impl FileRef for &str {
    fn refers_to_file(&self) -> Result<FileKnown, std::io::Error> {
        let fh = File::open(&self)?;
        Ok(
            FileKnown {
                fpath: &self,
                file: fh,
            }
        )
    }

    fn refers_to_file_at(&self) -> &str {
        &self
    }
}

impl FileRef for FileKnown<'_> {
    fn refers_to_file(&self) -> Result<FileKnown, std::io::Error> {
        let fh = File::open(&self.fpath)?;
        Ok(
            FileKnown {
                fpath: &self.fpath,
                file: fh,
            }
        )
    }

    fn refers_to_file_at(&self) -> &str {
        &self.fpath
    }
}

#[derive(Debug)]
struct FileKnown<'a> {
    fpath: &'a str,
    file: File,
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Clone)]
enum Reg {
    Base(RegBase),
    PtrSum(Vec<RegBase>),
    PtrArithOther(Vec<RegBase>),
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Clone)]
enum RegBase {
    Named(String),
    Fixed(isize),
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Clone)]
struct RegRef {
    register: Reg,
    location: usize,
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Clone)]
struct Op(String);
